# Demo

This is a repository that showcases Gitlab pipelines and Docker.

***NOTE: This project was created using respective cli commands, and setup to use VS Code.***

# Overview

Gitlab pipelines is a versatile way of building source code from git and create artifacts that can be deployed in various ways, this project will showcase some of the concepts that can be used.

Some requirements are required on the developer system to be able to run some of the commands, these are:

- Docker Desktop
- git
- dotnet core 2.2 sdk
- nodejs
- angular cli
- sass


# Local Running

## Docker Compose Application

Simply run from the root of the project:

    docker-compose up -d --build

This will create the web and api containers on a network, when complete open the browser and go to http://localhost:81 for the web project and http://localhost:8080/swagger for the api.

## Build the solution

dotnet build src/Demo.sln

## Run tests

### API

From the root of the repository run:

    dotnet test src/Demo.sln

### Web

Navigate to the folder src/Demo.Web in a terminal and then run:


    ng test 

# Build pipeline

## Continuous Delivery

## Targeted Builds

